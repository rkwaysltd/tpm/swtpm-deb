#!/bin/sh
gpg --batch --gen-key ./rkwaysltd-swtpm-deb-gpg.batch
# --secret-keyring ignored in gpg2 - left as example for gpg1
gpg --no-default-keyring --secret-keyring ./rkwaysltd-swtpm-deb.key --keyring ./rkwaysltd-swtpm-deb.pub --list-secret-keys
# make ./rkwaysltd-swtpm-deb.key with gpg2
gpg --no-default-keyring --secret-keyring ./rkwaysltd-swtpm-deb.key --keyring ./rkwaysltd-swtpm-deb.pub --export-secret-keys > ./rkwaysltd-swtpm-deb.key.tmp
mv ./rkwaysltd-swtpm-deb.key.tmp ./rkwaysltd-swtpm-deb.key
gpg --no-default-keyring --secret-keyring ./rkwaysltd-swtpm-deb.key --keyring ./rkwaysltd-swtpm-deb.pub --export-secret-keys --armor > ./rkwaysltd-swtpm-deb.key.asc
gpg --import ./rkwaysltd-swtpm-deb.key
gpg --no-default-keyring --output ./rkwaysltd-swtpm-deb.pub.asc --armor --export --keyring ./rkwaysltd-swtpm-deb.pub
