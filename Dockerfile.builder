FROM library/debian:10
ENV SWTPM_VERSION=0.5.2
ENV LIBTPMS_VERSION=0.8.2
COPY swtpm.gpg-key.asc .
RUN apt-get update \
 && apt-get install -y \
    build-essential \
    debhelper \
    apt-utils \
    dh-exec \
    curl \
    libfuse-dev \
    libglib2.0-dev \
    libgmp-dev \
    libtasn1-dev \
    gnutls-dev \
    libssl-dev \
    libseccomp-dev \
    expect \
    socat \
    gnutls-bin \
    python3-setuptools \
    python3-twisted \
    gawk \
    bzip2 \
    xz-utils \
    net-tools \
    softhsm2 \
 && gpg --import swtpm.gpg-key.asc \
 && curl -sSLf https://github.com/stefanberger/swtpm/archive/refs/tags/v${SWTPM_VERSION}.tar.gz -o /usr/src/swtpm-${SWTPM_VERSION}.tar.gz \
 && curl -sSLf https://github.com/stefanberger/swtpm/releases/download/v${SWTPM_VERSION}/v${SWTPM_VERSION}.tar.gz.asc -o /tmp/swtpm.tar.gz.asc \
 && curl -sSLf https://github.com/stefanberger/libtpms/archive/refs/tags/v${LIBTPMS_VERSION}.tar.gz -o /usr/src/libtpms-${LIBTPMS_VERSION}.tar.gz \
 && curl -sSLf https://github.com/stefanberger/libtpms/releases/download/v${LIBTPMS_VERSION}/v${LIBTPMS_VERSION}.tar.gz.asc -o /tmp/libtpms.tar.gz.asc \
 && gpg --verify /tmp/swtpm.tar.gz.asc /usr/src/swtpm-${SWTPM_VERSION}.tar.gz \
 && gpg --verify /tmp/libtpms.tar.gz.asc /usr/src/libtpms-${LIBTPMS_VERSION}.tar.gz \
 && rm -rf /tmp/swtpm.tar.gz.asc /tmp/libtpms.tar.gz.asc /var/lib/apt/lists/*
ARG BUILD_DATE
ARG REVISION
LABEL org.opencontainers.image.revision="$REVISION" \
    org.opencontainers.image.created="$BUILD_DATE"
