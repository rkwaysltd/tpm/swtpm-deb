# swtpm-deb

Debian repository for swtpm - https://github.com/stefanberger/swtpm

# Adding repository

Please run the following command:

```sh
curl -sfL https://rkwaysltd.gitlab.io/tpm/swtpm-deb/rkwaysltd-swtpm-deb.pub.asc | sudo apt-key add -
echo "deb https://rkwaysltd.gitlab.io/tpm/swtpm-deb/ buster/" | sudo tee -a /etc/apt/sources.list.d/rkwaysltd-swtpm.list
sudo apt update
```

# Repository key

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBGCbY0QBEADFcBMwPY5nzx/t8GaIlLdz315VSL2ijgxa0yDYTQNQcW63AeDO
BCsdcldq5l+SccyIHiSXISd/e+OufHLzwngM//+dZfpyh1ZsZ/mrjr4vX1jxKfX0
SwsOJP735GKhcNZA9wFwALOIfQ+mwuVmlBSiSqBwlRVCfu6vGZ5zCrH7W4CNjbqO
s/7wSa53sSJsWBhHgEPdW4AZ0pLx9vuj91tr0V0QHxRKF2EZtCrEhYi9jPvzMA1A
Ty8I0AXeXGK0wv/IxqNssYU7yQeYaeu+NmV9lF11PW3nd0HfthbtZbk+cMtDYo1G
/Rq+B/V3XI33D9p1uJQc2Mvaz8GCYh8wZVU/TpF5zX0cvqvkjxO8ALGgLWVsFSTR
K4tpGTsh3hvw+pD6IlBufFhuQ6HHVVFsQjCeJwT4aw/EX8hpnuPcwXUgbQSlpDDC
BDdPxhgOfFeLAu9nbA0anveQvCB8n4yfSixWDV87+vyDtmNi4JwoMSrRaNvur31x
VudbyVZABBngR1oi/Z60FbbIi4NHFfLbfDL+NwsvMyVF4OjIm4FFBoouZrSJR8Ew
HcdEg7m3DEusJwVWFj5h6j5FnjSFzLpxSCxGWkNlMu7bEniEBYRJX5fO5bF/T8Ew
7IGErpgR7i3ed2K6E+SuK84/aUTfKKwmdjcw0iExbGhfefKI+aht2iJsUQARAQAB
tF5SS3dheXMgTHRkLiBodHRwczovL2dpdGxhYi5jb20vcmt3YXlzbHRkL3RwbS9z
d3RwbS1kZWIgRGViaWFuIFJlcG9zaXRvcnkgPHJrd2F5c2x0ZEBnbWFpbC5jb20+
iQJOBBMBCAA4FiEEYEHtEgTQUcHJte8rt0nWG9u8T4sFAmCbY0QCGy8FCwkIBwIG
FQoJCAsCBBYCAwECHgECF4AACgkQt0nWG9u8T4tCQBAAn7hTmVXc33WW8wOlT8ou
6W4zZbVHldk2vR/npT4Kw3cGlUemyKFeaPlsG979WQqVU5ltbjoGVWyeX5u/RSpe
EnHm/jvGFOoDAg8qXdYkPO9myt7p1uqTUL4efrffKj67B0IuDfiPoTMfohv5326Q
K9rrwY5fhDv5Ld7AITaS9Xt4QchxQagFFCVDBLn15wwNycJA9LuaPOID63GNe3tV
q/hvpcyZXmoYJpPVdp1jSyR33MQgQyeD9XymCNXomXjoXIgSu3VRKRZ3wGTJMYHg
VgiaYOK4+xsdH/mFAJhWdUdpvAO8aVGG+AXiB1Lde+O1Hcvg7gXGzPz4i6VTU1PA
K4UqEjpKqOojjaMCTOZkBuEZhB1GHMxAgiRxV9L/V8BTUzyWJAVGT5RH1HZrJC5y
YiyGAzB5qQuPfciavdIvY8ruBcFesiALvqpD4ruc1W/Sh+RT0eoqJYHklv1HjVo/
h3wAL8cVitPo5q/VQJ9SSO3ITDs+IHCOSyZsx6kkRcxvz1tw2NIr9/d0rkxohQ1G
ucXUI9fNKVOCzRMK3mNaQd8qHOZ2HRxc/1xwTkh/eta8BUhX3/7TKdcCEJAkL+8m
wAe48hCFXKZd/nbNqrZQjCMYObbHjTTvt2o8adJIYexFN9UgIrJvk+PDmh4mQRcB
yAGe0q2niug/ze1tckjSa00=
=Gq7O
-----END PGP PUBLIC KEY BLOCK-----
```

# Gitlab CI variables

- `PROJ_CI_GPG_KEY_FILE` (type: File, protected) - GPG secret key for repository signing (content of `rkwaysltd-swtpm-deb.key.asc` - see `rkwaysltd-swtpm-deb-gpg.sh`)
